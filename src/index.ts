import 'dotenv/config';
import mongoose from 'mongoose';
import path from 'path';
import { Context, Telegraf } from 'telegraf';
import TelegrafI18n, { I18n } from 'telegraf-i18n';
import { session } from 'telegraf-session-mongodb';
import { DEFAULT_LOCALE } from './config';
import { checkChatMember, checkGroupChat, checkPollCreator, findPoll } from './middleware';
import { Poll } from './models';
import { handleCloseConfirmation, handleCloseRejection, handlePollClose } from './polls/pollClose';
import { handleInlineQuery, handlePollCommand } from './polls/pollComand';
import { handlePollVotes } from './polls/pollVotes';

export interface SessionContext extends Context {
    session: any;
    i18n: I18n,
    state: {
        poll?: Poll
    }
};

// Check for required environment variables
if (!process.env.BOT_TOKEN) {
    throw new Error('BOT_TOKEN environment variable is required');
}
if (!process.env.MONGODB_URI) {
    throw new Error('MONGODB_URI environment variable is required');
}

// Connect to MongoDB
const bot = new Telegraf<SessionContext>(process.env.BOT_TOKEN);
mongoose.connect(process.env.MONGODB_URI);

mongoose.connection.once('open', () => {
    console.log("DB connection established!");

    bot.use(session(mongoose.connection.db));

    // Initialize telegraf-i18n
    const i18n = new TelegrafI18n({
        defaultLanguage: DEFAULT_LOCALE,
        useSession: true,
        allowMissing: false,
        directory: path.resolve(__dirname, 'locales'),
    });

    // Use the telegraf-i18n middleware
    bot.use(i18n.middleware());

    bot.use((context, next) => {
        const lang = context.message?.from?.language_code;
        context.i18n.locale(lang);
        next();
    });

    // Handle the /poll command
    bot.command('poll', checkGroupChat, (ctx) => handlePollCommand(ctx as SessionContext, false));

    // Handle the /anonpoll command
    bot.command('anonpoll', checkGroupChat, (ctx) => handlePollCommand(ctx as SessionContext, true));

    bot.on('inline_query', handleInlineQuery);

    bot.action(/^vote(\d+)$/, checkGroupChat, checkChatMember, findPoll, handlePollVotes);
    bot.action('close', checkGroupChat, findPoll, checkPollCreator, handlePollClose);
    bot.action('confirmClose', checkGroupChat, findPoll, checkPollCreator, handleCloseConfirmation);
    bot.action('rejectClose', checkGroupChat, findPoll, checkPollCreator, handleCloseRejection);

    bot.launch().then(() => console.log("Telegraf is ready!"));
});

bot.catch(console.log);