import mongoose, { Document, Schema } from 'mongoose';

// Define the Poll interface
export interface Poll extends Document {
  question: string;
  options: string[];
  votes: number[];
  voters: number[][];
  chatId: number;
  messageId: number;
  anonymous: boolean;
  open: boolean;
  creatorId: number | null;
  language: string | null;
}

// Define the Poll schema
export const pollSchema = new Schema<Poll>({
  question: String,
  options: [String],
  votes: [Number],
  voters: [[Number]],
  chatId: Number,
  messageId: Number,
  anonymous: { type: Boolean, default: false },
  open: { type: Boolean, default: true },
  creatorId: { type: Number, default: null },
  language: { type: String, default: null },
});

// Create the Poll model
export const PollModel = mongoose.model<Poll>('Poll', pollSchema);
