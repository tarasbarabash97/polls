import { InlineQueryResultArticle, InputTextMessageContent } from 'telegraf/typings/telegram-types';
import { SessionContext } from '..';
import { PollModel } from '../models';
import { formatPollResultMessage } from './pollFormatting';

/**
 * Handle the /poll command.
 * @param ctx - The Telegraf context.
 * @param anonymous - Whether the poll should be anonymous or not.
 */
export async function handlePollCommand(ctx: SessionContext, anonymous = false) {
    // Parse the arguments
    const { question, options } = parsePollInput(ctx.message!.text);

    // Validate the input
    if (!question || !options || options.length < 2) {
        return ctx.reply(ctx.i18n.t('poll.invalidInput'));
    }

    // Check if the question is an empty string
    if (question.trim() === '') {
        return ctx.reply(ctx.i18n.t('poll.emptyQuestion'));
    }

    // Check if any of the options are empty strings
    if (options.some((option) => option.trim() === '')) {
        return ctx.reply(ctx.i18n.t('poll.emptyOption'));
    }

    // Create a new poll
    const poll = new PollModel({
        question,
        options,
        votes: new Array(options.length).fill(0),
        voters: new Array(options.length).fill([]),
        chatId: ctx.chat!.id,
        messageId: undefined,
        anonymous,
        open: true,
        creatorId: ctx.from!.id,
        language: ctx.from!.language_code, // store the language of the user who created the poll
    });

    // Format the poll result message
    const { messageText, keyboard } = await formatPollResultMessage(ctx, poll);

    // Send the poll message
    const message = await ctx.replyWithHTML(messageText, {
        reply_markup: { inline_keyboard: keyboard },
    });

    // Update the poll document with the message ID
    poll.messageId = message.message_id;
    await poll.save();
}

/**
 * Handle inline queries.
 * @param ctx - The Telegraf context.
 */
export async function handleInlineQuery(ctx: SessionContext) {
    const { question, options } = parsePollInput(ctx.inlineQuery!.query);

    // Check if the question is an empty string
    if (question && question.trim() === '') {
        return await ctx.answerInlineQuery([
            {
                type: 'article',
                id: 'emptyQuestion',
                description: ctx.i18n.t('poll.emptyQuestionDescription'),
                title: ctx.i18n.t('poll.emptyQuestion'),
                input_message_content: {
                    message_text: ctx.i18n.t('poll.emptyQuestion'),
                } as InputTextMessageContent,
            },
        ]);
    }

    // Check if any of the options are empty strings
    if (options && options.some((option) => option.trim() === '')) {
        return await ctx.answerInlineQuery([
            {
                type: 'article',
                id: 'emptyOption',
                title: ctx.i18n.t('poll.emptyOption'),
                description: ctx.i18n.t('poll.emptyOptionDescription'),
                input_message_content: {
                    message_text: ctx.i18n.t('poll.emptyOption'),
                } as InputTextMessageContent,
            },
        ]);
    }

    if (!question || !options || options.length < 2) {
        const results: InlineQueryResultArticle[] = [];
        if (!question) {
            results.push({
                type: 'article',
                id: 'noQuestion',
                title: ctx.i18n.t('poll.noQuestion'),
                description: ctx.i18n.t('poll.noQuestionDescription'),
                input_message_content: {
                    message_text: ctx.i18n.t('poll.noQuestion'),
                } as InputTextMessageContent,
            });
        }
        if (!options || options.length < 2) {
            results.push({
                type: 'article',
                id: 'noOptions',
                title: ctx.i18n.t('poll.noOptions'),
                description: ctx.i18n.t('poll.noOptionsDescription'),
                input_message_content: {
                    message_text: ctx.i18n.t('poll.noOptions'),
                } as InputTextMessageContent,
            });
        }
        return await ctx.answerInlineQuery(results);
    }
    const results: InlineQueryResultArticle[] = [
        {
            type: 'article',
            id: 'public',
            title: ctx.i18n.t('poll.createPublic'),
            description: ctx.i18n.t('poll.createPublicDescription'),
            input_message_content: {
                message_text: `/poll "${question}" ${options.join(', ')}`,
            } as InputTextMessageContent,
        },
        {
            type: 'article',
            id: 'anonymous',
            title: ctx.i18n.t('poll.createAnonymous'),
            description: ctx.i18n.t('poll.createAnonymousDescription'),
            input_message_content: {
                message_text: `/anonpoll "${question}" ${options.join(', ')}`,
            } as InputTextMessageContent,
        },
    ];
    await ctx.answerInlineQuery(results);
}

function parsePollInput(input: string) {
    let question: string | undefined;
    let options: string[] | undefined;
    // Check for the old syntax using double quotes, alternative double quotes, single quotes, and other symbols similar to quotes
    const args = input.split(/["“”'‘’‛❛❜⹂‚‹›«»]/);
    if (args.length > 1) {
        question = args[1];
        options = args[2]?.split(',').map((option) => option.trim());
    } else {
        const index = input.search(/[[{<]/);
        if (index !== -1) {
            question = input.slice(0, index).trim();
            const closingBracket = { '[': ']', '{': '}', '<': '>' }[input[index]];
            options = input.slice(index + 1, input.indexOf(closingBracket!)).split(',').map((option) => option.trim());
        } else {
            question = input.trim();
        }
    }

    return { question, options };
}
