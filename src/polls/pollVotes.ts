import { SessionContext } from '..';
import { formatPollResultMessage } from './pollFormatting';

/**
 * Handle poll votes.
 * @param ctx - The Telegraf context.
 */
export async function handlePollVotes(ctx: SessionContext) {
    // Parse the option index
    const optionIndex = parseInt(ctx.match![1]);

    // Get the poll from the context state
    const poll = ctx.state.poll!;

    // Reject votes if the poll is closed
    if (!poll.open) {
        try {
            return ctx.answerCbQuery(ctx.i18n.t('poll.closed'));
        } catch (err) {
            console.log("handlePollVotes,!poll.open", err);
        }
    }

    // Check if the user has already voted
    if (poll.voters.some((voters) => voters.includes(ctx.from!.id))) {
        try {
            return ctx.answerCbQuery(ctx.i18n.t('poll.alreadyVoted'));
        } catch (err) {
            console.log("handlePollVotes,poll.voters", err);
        }
    }

    // Register the vote
    poll.votes[optionIndex]++;
    poll.voters[optionIndex].push(ctx.from!.id);
    await poll.save();

    // Format the poll result message
    const { messageText, keyboard } = await formatPollResultMessage(ctx, poll);

    // Update the poll message
    await ctx.editMessageText(messageText, {
        parse_mode: 'HTML',
        reply_markup: { inline_keyboard: keyboard },
    });
}