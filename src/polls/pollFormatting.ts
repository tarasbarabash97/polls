import { InlineKeyboardButton } from 'telegraf/typings/telegram-types';
import { SessionContext } from '..';
import { Poll } from '../models';
import { DEFAULT_LOCALE } from '../config';

/**
 * Format the poll result message.
 * @param ctx - The Telegraf context.
 * @param poll - The poll document.
 */
export async function formatPollResultMessage(ctx: SessionContext, poll: Poll) {
    let messageText = '';

    // Set the language to that of the poll creator
    ctx.i18n.locale(poll.language || DEFAULT_LOCALE);

    // Add a "closed" label if the poll is closed
    if (!poll.open) {
        messageText += `<b>${ctx.i18n.t('poll.closedLabel')}</b>\n`;
    }

    messageText += `<b><i>${poll.question}</i></b>\n`;
    const keyboard: InlineKeyboardButton[][] = [];
    const totalVotes = poll.votes.reduce((a, b) => a + b, 0);

    for (let [index, option] of Object.entries(poll.options) as [[string | number, string]]) {
        index = parseInt(index as string);
        const voteCount = poll.votes[index];
        const votePercentage = totalVotes ? ((voteCount / totalVotes) * 100).toFixed(2) : 0;
        messageText += `\n${option} — ${voteCount} (${votePercentage}%)`;
        if (poll.open) keyboard.push([{ text: option, callback_data: `vote${index}` }]);

        // Only show voter names if the poll is not anonymous
        if (!poll.anonymous) {
            messageText += '\n' + (await Promise.all(poll.voters[index].map(async voterId => {
                const voter = await ctx.telegram.getChatMember(ctx.chat!.id, voterId);
                return `<a href="tg://user?id=${voterId}">${voter.user.username || voter.user.first_name
                    }</a>`;
            }))).join(", ");
        }

        if (!poll.anonymous && poll.options.length != index - 1 && poll.voters[index].length > 0) {
            messageText += "\n";
        }
    }

    // Add a button to allow the creator of the poll to close it
    if (poll.open && poll.creatorId != null) {
        keyboard.push([{ text: `${ctx.i18n.t('poll.close')} 🔒`, callback_data: 'close' }]);
    }

    // Add the total number of votes and percentage of chat members who voted
    const chatMemberCount = (await ctx.getChatMemberCount()) - 1; // subtract 1 to exclude the bot
    const votePercentage = chatMemberCount ? ((totalVotes / chatMemberCount) * 100).toFixed(2) : 0;

    messageText += `\n\n<b>${ctx.i18n.t('poll.totalVotes', { count: totalVotes })}</b>`;
    messageText += `\n<b>${ctx.i18n.t('poll.votePercentage', { percentage: votePercentage })}</b>`;

    return { messageText, keyboard };
}