import { SessionContext } from '..';
import { formatPollResultMessage } from './pollFormatting';

// Handle poll closure
export async function handlePollClose(ctx: SessionContext) {
    // Get the poll from the context state
    const poll = ctx.state.poll!;

    if (!poll.open) {
        try {
            return ctx.answerCbQuery(ctx.i18n.t('poll.alreadyClosed'));
        } catch (err) {
            console.log("handlePollClose,!poll.open", err);
        }
    }

    await ctx.reply(ctx.i18n.t('poll.confirmClose'), {
        reply_to_message_id: ctx.callbackQuery!.message!.message_id,
        reply_markup: {
            inline_keyboard: [
                [
                    { text: ctx.i18n.t('poll.yes'), callback_data: 'confirmClose' },
                    { text: ctx.i18n.t('poll.no'), callback_data: 'rejectClose' },
                ],
            ],
        },
    });
    try {
        return await ctx.answerCbQuery();
    } catch (err) {
        console.log("handlePollClose,end", err);
    }
}

export async function handleCloseConfirmation(ctx: SessionContext) {
    // Get the poll from the context state
    const poll = ctx.state.poll!;

    // Close the poll
    poll.open = false;
    await poll.save();

    // Format the poll result message
    const { keyboard, messageText } = await formatPollResultMessage(ctx, poll);

    // Update the poll message
    await ctx.telegram.editMessageText(
        ctx.chat!.id,
        poll.messageId,
        undefined,
        messageText,
        { parse_mode: "HTML", reply_markup: { inline_keyboard: keyboard } }
    );

    // Delete the confirmation message
    await ctx.deleteMessage();
}

export async function handleCloseRejection(ctx: SessionContext) {
    // Delete the confirmation message
    await ctx.deleteMessage();
}