import { Middleware as MiddlewareFn } from 'telegraf';
import { SessionContext } from '.';
import { PollModel } from './models';

/**
 * Middleware function to check if the action was sent in a group chat.
 */
export const checkGroupChat: MiddlewareFn<SessionContext> = async (ctx, next) => {
    if (ctx.chat?.type !== 'group' && ctx.chat?.type !== 'supergroup') {
        return;
    }
    await next();
};

/**
 * Middleware function to check if the user is a member of the group chat.
 */
export const checkChatMember: MiddlewareFn<SessionContext> = async (ctx, next) => {
    const member = await ctx.getChatMember(ctx.from!.id);
    if (member.status === 'left' || member.status === 'kicked') {
        return ctx.answerCbQuery(ctx.i18n.t('poll.mustBeMember'));
    }
    await next();
};

/**
 * Middleware function to find the associated poll.
 */
export const findPoll: MiddlewareFn<SessionContext> = async (ctx: any, next) => {
    const poll = await PollModel.findOne({
        chatId: ctx.chat!.id,
        messageId: (ctx.callbackQuery?.message.reply_to_message && ctx.callbackQuery?.message.reply_to_message.message_id) || ctx.callbackQuery!.message!.message_id,
    });
    if (!poll) {
        return ctx.answerCbQuery(ctx.i18n.t('poll.notFound'));
    }
    ctx.state.poll = poll;
    await next();
};

/**
 * Middleware function to check if the user is the creator of the poll.
 */
export const checkPollCreator: MiddlewareFn<SessionContext> = async (ctx, next) => {
    const poll = ctx.state.poll!;
    if (ctx.from!.id !== poll.creatorId) {
        return ctx.answerCbQuery(ctx.i18n.t('poll.notCreator'));
    }
    await next();
};